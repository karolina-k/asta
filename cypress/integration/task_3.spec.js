describe('Test task_3', () => {
  beforeEach(() => {
    cy.visit('/task_3')
      .get('#main-reset')
      .click();
  });

  const name = 'Taylor';
  const surname = 'Swift';
  const note = 'Taylor Alison Swift is an American singer-songwriter.';

  it('should not allow to edit form', () => {
    cy.get('.form-control').each($input => {
      //expect($input.prop('disabled')).to.equal(true);

      cy.wrap($input)
        .invoke('prop', 'disabled')
        .should('be.true');
    });

    cy.get('#in-file')
      .invoke('prop', 'disabled')
      .should('be.true');
  });
  it.only('should allow to edit form', () => {
    cy.get('.menu-border').click();

    //cy.get('#menu1').trigger('mousein'); - it doesn't work properly

    cy.get('#start-edit').click({ force: true });

    //edit name
    cy.get('#in-name')
      .clear()
      .type(name);

    //edit surname
    cy.get('#in-surname')
      .clear()
      .type(surname);

    //add note
    cy.get('#in-notes').type(note);

    // upload file
    // const testFile = new File(['data to upload'], 'upload.txt');
    // cy.get('#in-file').trigger('change', { testFile, force: true });

    //save
    cy.get('#save-btn').click();

    //test custom notification
    cy.get('.notifyjs-corner')
      .should('be.visible')
      .and('contain', 'Twoje dane zostały poprawnie zapisane');

    //check fields after editing

    //check name
    cy.get('#in-name').should('have.value', name);

    //check surname
    cy.get('#in-surname').should('have.value', surname);

    //check note
    cy.get('#in-notes').should('have.value', note);
  });
});
