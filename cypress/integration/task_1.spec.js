describe('Test task_1', () => {
  beforeEach(() => {
    cy.visit('/task_1')
      .get('#main-reset')
      .click();
  });

  it('Add products to the basket', () => {
    //1st product
    //take the first product on the list and add 12 items
    cy.get('.basket-list').addNthProductToBasket(0, 12);

    //2nd product
    cy.get('.basket-list').addNthProductToBasket(1, 1);

    //3rd product
    cy.get('.basket-list').addNthProductToBasket(2, 4);

    //check the summary number of items in the basket
    cy.get('.summary-quantity')
      .invoke('text')
      .should('be.equal', '17');
  });

  it.only('should not allow to add more than 100 products', () => {
    cy.get('.thumbnail:eq(0) .form-control')
      .clear()
      .type('101')
      .should('have.value', '101');

    //click on add button
    //check window alert
    const stub = cy.stub();
    cy.on('window:alert', stub);

    cy.get('.input-group-btn:first button')
      .click()
      .then(() => {
        expect(stub.getCall(0)).to.be.calledWith(
          'Łączna ilość produktów w koszyku nie może przekroczyć 100.'
        );
      });
    // number of items of chosen product = 0
    cy.get('.form-control:first').should('have.value', '0');

    // the summary number of items in the basket = 0
    cy.get('.summary-quantity').should('contain', '0');
  });

  it('should allow to delete products in the basket', () => {
    cy.get('.basket-list').addNthProductToBasket(3, 15);

    //click on delete button
    cy.get('.panel-body .btn-sm').click();

    // the summary number of items in the basket should equal 0
    cy.get('.summary-quantity').should('contain', '0');
  });
});
