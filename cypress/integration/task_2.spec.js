describe('Test task_2', () => {
  beforeEach(() => {
    cy.visit('/task_2')
      .get('#main-reset')
      .click();
  });

  it('should show categories', () => {
    // open custom dropdown
    cy.get('.select2-container').click();

    const selectedTag = 'Elektronika';
    // choose wanted option
    cy.get('.select2-results__options li')
      .contains(selectedTag)
      .click();

    // check if all remaining products on the list have a proper tag
    cy.get('#product-list .thumbnail').each($product => {
      cy.wrap($product)
        .find('.caption strong')
        .should('have.text', selectedTag);
    });
  });
  it('should display hints while typing', () => {
    // open custom dropdown
    cy.get('.select2-selection__rendered').click();

    // type partial phrase
    const partialPhrase = 'fi';
    cy.get('.select2-search__field').type(partialPhrase);

    // this don't check if there are other elements not matching partialPharse
    // cy.get('.select2-results li')
    //   .should('contain', 'Firma i usługi')
    //   .click();

    // all hints contain the partial phrase
    cy.get('.select2-results li').each($hint => {
      // cy.wrap($hint).should('contain', partialPhrase); // is case-sensitive
      // expect($hint.text().toLowerCase()).to.contain(partialPhrase); // is not case-sensitive

      cy.wrap($hint)
        .invoke('text')
        .invoke('toLowerCase')
        .should('contain', partialPhrase);
    });
  });
});
