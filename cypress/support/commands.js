Cypress.Commands.add(
  'addNthProductToBasket',
  { prevSubject: 'element' },
  ($basket, productIdx, quantity) => {
    cy.wrap($basket).as('basket');

    // get nth product form the document
    cy.get('.thumbnail')
      .eq(productIdx)
      .within($product => {
        // set quantity on the chosen product
        const $button = $product.find('button');
        const name = $button.attr('data-product-name');
        const price = $button.attr('data-product-price');

        cy.get('.form-control')
          .clear()
          .type(quantity)
          .should('have.value', String(quantity));

        //click on add button
        cy.get('button').click();

        //check if the basket contains added product
        cy.get('@basket').should('contain', `${name} (${price})`);

        //check the number of added items
        cy.get('@basket')
          .find(`[data-quantity-for="${name}"]`)
          .should('have.text', `${quantity}`);
      });
  }
);
